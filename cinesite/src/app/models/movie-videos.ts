export class Video {
  id: number;
  key: string;
  name: string;
  site: string;
  type: string;
}


export class MovieVideos {
  id: number;
  results: Video[];
}
