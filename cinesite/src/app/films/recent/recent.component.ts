import {Component, OnDestroy, OnInit} from '@angular/core';
import {Movie} from "../../models/movie";
import {Subscription} from "rxjs";
import {DatabaseService} from "../../services/database.service";

@Component({
  selector: 'app-film-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.scss']
})
export class RecentComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  movies: Movie[];
  titleToAdd: string;
  displayedColumns = ['id', 'title'];

  constructor(private dbService:DatabaseService<Movie>) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies() {
    let observableMovies = this.dbService.getMovies();

    this.subscription.add(
      observableMovies.subscribe(data => {
        this.movies = data;
      })
    );
  }


  addMovie() {
    // Creation de l'objet Movie
    var newMovie = new Movie(this.titleToAdd);

    this.subscription.add(
    // Appel HTTP pour sauvegarder le film
    this.dbService.saveMovie(newMovie)
    // Gestion du retour du message HTTP
      .subscribe(newMovie => {
        // On vide le champ input
        this.titleToAdd = "";
        // On rafraichit la liste des films
        this.getMovies();
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
