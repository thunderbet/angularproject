import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-film-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {

  public value = "Laisser votre commentaire";

  constructor() { }

  ngOnInit() {
  }

  affiche() {
    alert(this.value);
  }

}
