import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RecentComponent} from "./films/recent/recent.component";
import {CategorieComponent} from "./films/categorie/categorie.component";
import {DiscoverComponent} from "./films/discover/discover.component";

const routes: Routes = [
  { path: 'films/recents', component: RecentComponent },
  { path: 'films/categories', component: CategorieComponent },
  { path: 'films/discover', component: DiscoverComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
