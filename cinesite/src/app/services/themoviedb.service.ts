import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Movie} from "../models/movie";
import {DiscoverMovies} from "../models/discover-movies";
import {MovieVideos} from "../models/movie-videos";
import {forEach} from "@angular/router/src/utils/collection";

@Injectable({
  providedIn: 'root'
})
export class ThemoviedbService {
  private apiURL: string;
  private apiKey : string;
  private version = 3;

  constructor(private httpClient: HttpClient) {
    this.apiURL = environment.movieApi + this.version + "/";
    this.apiKey = environment.movieDbApiKey;
  }

  /**
   * Methode qui retourne la liste des films discovery
   */
  public getDiscoverMovies(page :number): Observable<DiscoverMovies> {
    var url = this.apiURL + "discover/movie";

    var httpParams = new HttpParams();
    httpParams = httpParams.set("api_key", this.apiKey);
    httpParams = httpParams.set("language", "en-US");
    httpParams = httpParams.set("sort_by", "popularity.desc");
    httpParams = httpParams.set("include_adult", "false");
    httpParams = httpParams.set("include_video", "false");
    httpParams = httpParams.set("page", page.toString());

    var httpOptions = {
      params: httpParams
    }

    return this.httpClient.get<DiscoverMovies>(url, httpOptions);
  }

  /**
   * Methode qui retourne la liste des films discovery
   */
  public getMovieVideos(movieId: number): Observable<MovieVideos> {
    var url = this.apiURL + "movie/" + movieId + "/videos";

    var httpParams = new HttpParams();
    httpParams = httpParams.set("api_key", this.apiKey);
    httpParams = httpParams.set("language", "en-US");

    var httpOptions = {
      // headers: httpHeaders,
      params: httpParams
    }

    return this.httpClient.get<MovieVideos>(url, httpOptions);
  }

  /**
   * Methode qui retourne la liste des films discovery
   */
  public getGenres(movieId: number): Observable<MovieVideos> {
    var url = this.apiURL + "genre/movie/list";

    var httpParams = new HttpParams();
    httpParams = httpParams.set("api_key", this.apiKey);
    httpParams = httpParams.set("language", "en-US");

    var httpOptions = {
      // headers: httpHeaders,
      params: httpParams
    }

    return this.httpClient.get<MovieVideos>(url, httpOptions);
  }
}
