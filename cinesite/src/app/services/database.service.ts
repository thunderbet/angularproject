import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import {environment} from "../../environments/environment";
import {promise} from "selenium-webdriver";
import delayed = promise.delayed;
import {delay} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class DatabaseService<Movie> {
  localUrl: string;

  constructor(private httpClient: HttpClient) {
    this.localUrl = environment.localDbApi;
  }

  /**
   * Methode qui retourne la liste des films
   */
  public getMovies(): Observable<Movie[]> {
      return this.httpClient.get<Movie[]>(this.localUrl);
  }

  /**
   * Methode qui sauvegarde un film, on passe le film en parametre
   * @param movie
   */
  public saveMovie(movie: Movie) : Observable<Movie> {
    return this.httpClient.post<Movie>(this.localUrl, movie);
  }

  /**
   * Methode pour effacer un film, on passe son id en parametre
   * @param movieId
   */
  public deleteMovie(movieId: number) {
    var url = (this.localUrl + movieId)
    return this.httpClient.delete<void>(url);
  }
}
