export const environment = {
  production: true,
  localDbApi: 'http://localhost:3000/movies/',
  movieDbApiKey: '4396a2abd282ceb0559f27bf820c4f35',
  movieApi: 'https://api.themoviedb.org/'
};
