# AngularProject


1) Installer NodeJS 10.x.x depuis https://nodejs.org/en/
    NodeJS est l'environnement d'execution des projet Angular.

2) Vérifier que Node est bien installé. Taper "node -v" dans la console

	> node -v
	> v10.15.3

3) Installer Angular CLI (Command Line Interface) qui va nous fournir des outils en ligne de commande pour faire des choses
   L'installation se fait avec npm en ligne de commande. NPM = Node Packet Manager c'est l'outil qui va nous aider pour gérer les dépendances du projet

	> npm install -g @angular/cli

4) Installer TypeScript
    > npm install -g typescript

5) Installer JsonServer
    > npm install -g json-server

6) Installer le logiciel WebStorm depuis https://www.jetbrains.com/webstorm/download/#section=windows.
   C'est un autre outil comme IntelliJ de JetBrains mais optimisé pour le web.

7) Charger le projet AngularProject dans le dossier git, au même niveau que notre projet webproject
    > git clone https://gitlab.com/thunderbet/angularproject.git

8) Ouvrir le projet dans WebStorm, ouvrir le terminal et taper :
    > cd cinesite
    > npm intall
    
    Ca va installer et téléchager toutes les dépendances du projet.


THE MOVIE DB

V3
4396a2abd282ceb0559f27bf820c4f35

V4
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0Mzk2YTJhYmQyODJjZWIwNTU5ZjI3YmY4MjBjNGYzNSIsInN1YiI6IjVjOThkNmRmOTI1MTQxMTA2N2Y0M2Q4YiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.V1zLUYjDnmko2E2pMGyCnbdx_GvC3NexoT-RLr_9Vck

https://api.themoviedb.org/3/list/1?api_key=4396a2abd282ceb0559f27bf820c4f35&language=en-US


# DEMARRER LE TOUT
    >npm run start